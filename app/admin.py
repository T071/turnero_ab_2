from django.contrib import admin
from . import models
from .forms import PersonaModelForm
#Register your models here.

class PersonaAdmin(admin.ModelAdmin):
    list_display = ["__str__", "nombres", "apellidos", "telefonos",  "fecha_creacion"]
    list_filter= [] #filtros de busqueda
    list_editable= [] #campos que pueden editarse
    search_fields= [] #campos de busqueda
    form = PersonaModelForm

admin.site.register(models.Personas)
admin.site.register(models.Servicios)
admin.site.register(models.Prioridades)
admin.site.register(models.BocasAtencion)
admin.site.register(models.Clientes)
admin.site.register(models.Usuarios)
admin.site.register(models.Turnos)