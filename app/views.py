from django.shortcuts import render
from .models import Personas
from .personaForm import PersonaFormulario

#Create your views here.

def Inicio (request):
   
   
   form = PersonaFormulario(request.POST or None)
   if form.is_valid():
        form_data = form.cleaned_data
        print(form_data)
        aux1 = form_data.get("documento")
        aux2 = form_data.get("nombres")
        obj = Personas.objects.create(numero_documento = aux1, nombres=aux2)
   context = {
        "titulo": 'Formulario',
        "el_form":form 
    }
   return render(request, "inicio.html", context)