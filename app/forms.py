from django import forms

from .models import Personas

class PersonaModelForm (forms.ModelForm):
    class Meta:
        model=Personas
        fields=["numero_documento"]

class PersonaFormulario(forms.Form):
    documento = forms.CharField(max_length=20, required=True)
    nombres = forms.CharField(max_length=100, required=True)